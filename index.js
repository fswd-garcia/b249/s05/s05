console.log("hello");

/*
	customer class
	-email property - string
	-cart property - instance of cart class
	-order property - empty array
	checkOut()method

*/

class Customer {
	constructor(email) {
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}
	checkOut(){
		// method
		// if cart not emplty, psuh order array {products: cart, totalAmount: cart total}
		if (this.cart !== ""){
			this.orders.push({
				products: this.cart, 
				totalAmount: this.cart.totalAmount
			})
		}
		return this;
		
	}
}

class Cart{
	constructor() {
		this.contents = [];
		this.totalAmount = 0;
	}
	addToCart(name, quantity){
		// method
		this.contents.push({
			product: name,
			quantity: quantity
		}) 
		return this;
	}
	showCartContents(){
		// method
		console.log(this.contents);
		return this;
	}
	updateProductQuantity(name, quantity){
		// method
		this.contents.forEach(product => {
			if(product.product.name === name){
				product.quantity = quantity
			}
		})
		return this;
	}
	clearCartContents(){
		// method
		this.contents = [];
		return this;
	}
	computeTotal(){
		// method
		let amount = 0;

		this.contents.forEach(product => {
			{
				// console.log('product.quantity ',product.quantity);
				// console.log('product.product.price ',product.product.price);
				amount = product.quantity * product.product.price;
			}
		})
		this.totalAmount = amount;
		return this;
	}
}
class Product{
	constructor(name, price) {
		this.name = name;
		this.price = price;
		this.isActive = true;
	}
	archive(){
		// method
		this.isActive = false;
		return this;
	}
	updatePrice(price){
		// method
		this.price = price;
		return this;
	}
}



// const john = new Customer('john@mail.com');
// john

// const prodA = new Product('soap', 9.99)
// prodA

// prodA.updatePrice(12.99)

// prodA.archive()

// const cart = new Cart()
// john.cart.addToCart(prodA, 3)

// john.cart.showCartContents()

// john.cart.updateProductQuantity('soap', 5)

// john.cart.clearCartContents()

// john.cart.computeTotal()

// john.checkOut()